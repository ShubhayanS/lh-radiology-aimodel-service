from flask import Flask,request,jsonify
from flask_restful import Api, Resource
import sys
import os
sys.path.append('../')
from services.coordinatesService import get_Coordinates

app = Flask(__name__)

@app.route('/',methods = ["GET","POST"])
# Get request returns the list of models
# Post request takes image and model name and returns coordinates of bounding box
def Boxes():
    if request.method == "GET":
        model_list = []
        for file in os.listdir("../lib"):
            if file.endswith(".py"):
                model_list.append(file.split('.')[0])
        return jsonify(model_list)
    elif request.method == "POST":
        img = request.files['image']
        model_name = request.files['model_name']
        return jsonify(get_Coordinates(img,model_name))

if __name__ == "__main__":
    app.run(debug=False)
