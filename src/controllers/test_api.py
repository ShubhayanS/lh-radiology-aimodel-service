import pytest
import requests
import json


url = "http://127.0.0.1:5000/"
def test_post() :
    data = {"image": open("../data/1_1.dcm", "rb"),"model_name" : "chexnet"}
    response = requests.post(url , files = data)
    assert response.status_code == 200
    
    
def test_get() :
    response = requests.get(url)
    assert response.status_code == 200
    