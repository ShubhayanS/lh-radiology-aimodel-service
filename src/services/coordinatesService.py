import sys
sys.path.append('../')
from io import StringIO 


def get_Coordinates(img,model_name):
    if model_name.read().decode("utf-8").lower() == "chexnet":
        from lib.cheXnet import getcoordinates
    return getcoordinates(img)
